package com.CustomerInfo.Model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "CustomerInfo")
public class CustomerModel {

	 @Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 private long customerNumber;
	 
	 @Column(name = "full_name", nullable = false)
	 private String fullName;
	 
	 @Column(name = "email", nullable = false)
	 private String email;
	 
	 @Column(name = "address", nullable = false)
	 private String address;
	 
	 @Column(name = "customer_type", nullable = false)
	 private String customerType;
		
	 public CustomerModel() {
			super();
			// TODO Auto-generated constructor stub
		}
	 
		public CustomerModel(long customerNumber, String fullName, String email, String address, String customerType) {
			super();
			this.customerNumber = customerNumber;
			this.fullName = fullName;
			this.email = email;
			this.address = address;
			this.customerType = customerType;
		}
		public long getCustomerNumber() {
			return customerNumber;
		}
		public void setCustomerNumber(long customerNumber) {
			this.customerNumber = customerNumber;
		}
		public String getFullName() {
			return fullName;
		}
		public void setFullName(String fullName) {
			this.fullName = fullName;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getCustomerType() {
			return customerType;
		}
		public void setCustomerType(String customerType) {
			this.customerType = customerType;
		}

	    
	    
}
