package com.CustomerInfo.Controller;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.ResponseEntity;
import com.CustomerInfo.Exception.ResourceNotFoundException;


import com.CustomerInfo.Repository.CustomerInfoRepository;
import com.CustomerInfo.Model.CustomerModel;

@RestController
//@RequestMapping("/customers")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerInfoController {
	
	
    @Autowired
    private CustomerInfoRepository customerInfoRepository;
    

	@PostMapping("/addCustomerInfo")
	    public CustomerModel createCustomer( @RequestBody CustomerModel customer) {
	        return customerInfoRepository.save(customer);
	    }

    @GetMapping("/customers")
    public List<CustomerModel> getAllCustomers() {
        return customerInfoRepository.findAll();
    }
    
    @GetMapping("/customers/{id}")
    public ResponseEntity<CustomerModel> getCustomerById(@PathVariable(value = "id") long customerId)
        throws ResourceNotFoundException {
    	CustomerModel customer = customerInfoRepository.findById(customerId)
          .orElseThrow(() -> new ResourceNotFoundException("Customer not found " + customerId));
        return ResponseEntity.ok().body(customer);
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<CustomerModel> updateCustomer(@PathVariable(value = "id") long customerId,
        @RequestBody CustomerModel customerDetails) throws ResourceNotFoundException {
    	CustomerModel customer = customerInfoRepository.findById(customerId)
        .orElseThrow(() -> new ResourceNotFoundException("Customer not found:: " + customerId));

    	customer.setFullName(customerDetails.getFullName());
    	customer.setEmail(customerDetails.getEmail());
    	customer.setAddress(customerDetails.getAddress());
    	customer.setCustomerType(customerDetails.getCustomerType());
    	
    	
        final CustomerModel updatedCustomer = customerInfoRepository.save(customer);
        return ResponseEntity.ok(updatedCustomer);
    }

    @DeleteMapping("/customers/{id}")
    public Map<String, Boolean> deleteCustomer(@PathVariable(value = "id") long customerId)
         throws ResourceNotFoundException {
    	CustomerModel customer = customerInfoRepository.findById(customerId)
       .orElseThrow(() -> new ResourceNotFoundException("Customer not found " + customerId));

        customerInfoRepository.delete(customer);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

	
	  
	  


}
