package com.CustomerInfo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.CustomerInfo.Model.CustomerModel;

public interface CustomerInfoRepository extends JpaRepository<CustomerModel, Long>{

}
