

import React, { useState, useEffect } from "react";
import { Table, Col, Row } from "reactstrap";
import {DeleteOutline} from '@material-ui/icons';

export const OutputCustomerList = props => {
    const { customerList } = props;
    const [hasError, setErrors] = useState(false);
    const [allCustomers, setAllCustomers] = useState([{}]);

    useEffect(() => {
        const res = fetch("http://localhost:8050/customers", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then(res => res.json())
            .then(res => setAllCustomers(res)).catch(err => setErrors(err));

    }, []);
    return (
        <div className="mt-4">
            <Row>
                <Col sm="12" md={{ size: 8, offset: 2 }}>
                    <Table hover>
                        <thead>
                            <tr>
                                {/* <th>#</th> */}
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Customer Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            <RenderTableData customerList={allCustomers} />
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </div>
    );
};
const RenderTableData = props => {
    const [hasError, setErrors] = useState(false);
    const finalArray = props.customerList;


    async function deleteData(id) {
        const res = await fetch("http://localhost:8050/customers"+id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then(res => res.json())
            .then(res =>JSON.stringify(true) ).catch(err => setErrors(err));
    };

    return finalArray.map((item, index) => {
        return (
            <tr key={index}>
                <td>{item.fullName}</td>
                <td>{item.email}</td>
                <td>{item.aaddress}</td>
                <td>{item.customerType}</td>

                <td>{item.lastName}</td>
                {/* <td><DeleteOutline onClick={()=>deleteData(item.customerNumber)}/></td> */}
            </tr>
        );
    });
};