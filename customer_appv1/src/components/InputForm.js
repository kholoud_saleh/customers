import React, { useState, useEffect } from "react";
import { Table, Col, Icon, Form, FormGroup, Label, Input, Row, Button } from "reactstrap";
// import {DeleteOutline, EditOutlined} from '@material-ui/icons';

export const InputForm = props => {
    const { updateCustomerListArray } = props;
    const initialInputState = { fullname: "", email: "", address:"", customerType:"" };
    const [eachEntry, setEachEntry] = useState(initialInputState);
    const { fullname, email , address, customerType} = eachEntry;
    const [hasError, setErrors] = useState(false);
    const [addCustomer, setAddCustomer] = useState([{}]);

    const handleInputChange = e => {
        setEachEntry({ ...eachEntry, [e.target.name]: e.target.value });
    };

    const handleFinalSubmit = e => {
        console.log("++++++++++++++++++++++++++++++++++++++++++", eachEntry)
        updateCustomerListArray(eachEntry)
        addData();
    };

    async function addData() {
        const res = await fetch("http://localhost:8050/addCustomerInfo", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                fullName: eachEntry.fullname,
                email: eachEntry.email,
                address: eachEntry.address,
                customerType: eachEntry.customerType,


            })
        })
            .then(res => res.json())
            .then(res => setAddCustomer(res)).catch(err => setErrors(err));
    };
    return (
        <div>
        <Row>
            <Col sm="12" md={{ size: 6, offset: 3 }} className="text-center">
                <h2> Add New Customer</h2>
            </Col>
        </Row>
        <Row className="mt-4">
            <Col sm="12" md={{ size: 6, offset: 3 }}>
                <Form>
                    <FormGroup>
                        <Label for="fullname">Full Name</Label>
                        <Input
                            name="fullname"
                            placeholder="Full Name"
                            onChange={handleInputChange}
                            value={fullname}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input
                            name="email"
                            placeholder="Email"
                            onChange={handleInputChange}
                            value={email}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="lastname">Address</Label>
                        <Input
                            name="address"
                            placeholder="Address"
                            onChange={handleInputChange}
                            value={address}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="lastname">Customer Type</Label>
                        <Input
                            name="customerType"
                            placeholder="Customer Type"
                            onChange={handleInputChange}
                            value={customerType}
                        />
                    </FormGroup>
                  
                    <Button onClick={handleFinalSubmit}>
                        Submit
                    </Button>
                </Form>
            </Col>
        </Row>
    </div>
    );
};
