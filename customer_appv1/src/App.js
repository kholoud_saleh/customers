import React, { useState } from "react";
import { InputForm } from "./components/InputForm";
import { OutputCustomerList } from "./components/OutputCustomerList";

function App() {
  const [customerList, setCustomerList] = useState([]);
  const updateCustomerListArray = eachEntry => {
    setCustomerList([...customerList, eachEntry]
    );
};
return (
  <div className="container mt-4">
    <InputForm updateCustomerListArray={updateCustomerListArray} />
    <OutputCustomerList customerList={customerList} />
  </div>
  );
}

export default App;
